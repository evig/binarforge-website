window.onload = function(){
    // find all placeholders
    var imagesToLoad = document.getElementsByClassName('image-placeholder');

    for(var idx in imagesToLoad){
        if(!imagesToLoad.hasOwnProperty(idx))
            continue; 

        var current = imagesToLoad[idx];

        // read the big image src
        var imgSrc = current.getAttribute('data-src');

        // load the big image
        var img = new Image();
        img.src = imgSrc;

        var imgElement = current.getElementsByClassName('mini')[0];

        if(imgElement){
            img.ele = imgElement;
            img.curr = current;
            img.swap = function(){
                this.ele.src = this.src;
                this.ele.className = 'image-loaded';
                this.curr.className = this.curr.className.replace('image-placeholder', '');
            };
        }
        else{
            img.ele = current;
            // has to be background-image then
            img.swap = function(){
                this.ele.style.backgroundImage = 'url('+this.src+')';
                this.ele.className += ' image-loaded';
            };
        }

        // swap and transit using css
        img.onload = function(){
            this.swap();
        };
    }
}