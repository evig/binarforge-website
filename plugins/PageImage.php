<?php

class PageImage extends AbstractPicoPlugin {

   private $ext = ".png";
   private $postfix = "thumbs/";

   public function onConfigLoaded(&$realConfig)
   {
      if(isset($realConfig['mcb_pageimage_ext'    ])) $this->ext     = $realConfig['mcb_pageimage_ext'];
      if(isset($realConfig['mcb_pageimage_postfix'])) $this->postfix = $realConfig['mcb_pageimage_postfix'];

	   $this->contenDir = '../'.str_replace($this->getRootDir(), "", $realConfig['content_dir']);
   }

   public function onSinglePageLoaded(&$pageData)
   {
        $id = str_replace(" ", "%20", $pageData['id']);
        $contentDir = '/assets/';

        $pageData['img'] = $contentDir.$id.$this->ext;
        $pageData['thumb'] = $contentDir.$this->postfix.$id.$this->ext;
   }
}
