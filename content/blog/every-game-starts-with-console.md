---
Title: Every game starts with... command console?
Description: I do blame command consoles in general for dragging me into programming.
Author: Bartlomiej Karalus
Date: 12/05/2016
Position: 9996
---

<div class="image-placeholder" data-src="%base_url%/assets/blog/every-game-starts-with-console.png">
    <img src="%base_url%/assets/thumbs/blog/every-game-starts-with-console.png" class="mini" />
</div>

I do blame command consoles in general for dragging me into programming. I remember that  feeling, while playing the legendary Quake ages ago, I have discovered the in-game console. At first it did look innocent but after a short time I was totally absorbed by the power it gives you. Sure, rocket launcher could be considered as a powerful stuff, but world-controlling command input? I was sold! 

That was probably the very first step of me learning how „these things” are actually done. How a virtual world is created and how can you affect it. Since that moment, every other game I played had to be examinated preciseluy by me looking for the way to activate the console or any other way to amend the way it works.

Some time later, when I finally got into development, I gradually realized how useful it can be from a creator point of view. Surely there are games for which having this kind of a gadget does not really bring any value, like very simple games for instance. Especially when creating a functioning console takes a half or more of the total dev time.
For example, in one of my previous jobs I was working on a strategy game. The goal that we have set was to make the game fully playable (in terms of mechanics) in the „headless” mode. 

It might sound nonsense, but we have quickly spotted how tidy architecture it can provide. What basically takes place is an absolute separation of concerns between the game logic and the game interface and input. This is because without having a single piece of UI, you should be able to run the game in command line mode and play the game using just command, e.g.:

&gt; load level1
&gt; goto walking-unit-1 33 44   (move an unit to field 33,44 on the level’s 1 map )

The whole game can be functioning and be complete as a core and after that you can „decorate” it with any kind of presentation you need. It can be either simple 2D map of sprites just following the data from the game or like our strategy simulator the scene can contain fully animated 3D units and buildings, still following the logic calculated by the game. Is it me or is that really a nice and clean approach?

Going further, when it comes to testing and debugging. Using a sequence of commands it seems pretty easy and painless to reproduce particular scenarios in the game in order to test/balance/debug the behaviour. 

And finally, adding GUI to your game should mostly contain of binding the on-screen widgets to specific commands. Spawning a building on the map should be as easy as storing the ID of the building that was just clicked on the list and then on a mouse click firing command like:

&gt; build &lt;object-id&gt; x, y

That was my brief view on command consoles in games and how useful and inspiring those devil's tools are.