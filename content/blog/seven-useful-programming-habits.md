---
Title: Seven useful programming habits
Description: These I have found insanely useful on my programming path but forming them is not an overnight change(...)
Author: Bartlomiej Karalus
Date: 08/07/2017
Position: 9992
---
 
<div class="image-placeholder" data-src="%base_url%/assets/blog/seven-useful-programming-habits.png">
    <img src="%base_url%/assets/thumbs/blog/seven-useful-programming-habits.png" class="mini" />
</div>

I have been reading some good books on forming habits recently. After digesting these, my mind started drifting further and I started thinking what my current habits are. Some of them are applicable to life in general, some relate only to work. Without surprise, some of them happen to be strongly related to programming, which I think might be a good thing to share. 

- **Uncontrolled auto save**. This one has been accompanying me for ages. Even though many modern IDEs do not even require saving a file, I am relentlessly squeezing "Ctrl + S" combination to its last…drop. If I recall correctly, I am doing it thoughtlessly every time I stop typing. Weird but it actually saved my day more times than it caused a smirk on my colleagues faces.
- Some devs tend to say being in "the zone" is like being in Nirvana or finally reaching the Valhalla. The way I see it is more like Berserk mode. It is great for some time but then you should take a break to recover. So basically short zone bursts are great for performance but make sure you **take regular stops**. Being in a zone for too long can actually numb your senses and make your mind more vulnerable to get stuck in a loop. (no pun intended!)
- Make sure you **kill all the sources of disturbance**. When I work on something really important I will turn my phone off, avoid social media or any unnecessary media in general with a slight exception for music. Apply everything in healthy limits though. If you have got kids and need to focus, locking them up in a basement might sound appealing but it is not really a good solution in longer term. 
- Always try to **start with an end in your mind**. Some people say the power of visualisation is priceless. It helps me determining realistic list of goals for today and eventually leads to reducing or removing frustration and disappointment at the end of the day. So anytime you work on something, make sure you know exactly what is it that you want to create. It might sound obvious but it is really one of those steps being skipped way too often.
- One good habit for me is **regular training**. Even though going to the gym is another great habit, in this case I am more worried about one's actual programming skills. I enjoy solving occasional programming exercises in order to keep my saw sharp all the time. It might not pay your bills but will definitely pay back in future. 
- One of my most recent ones is trying to **start writing any code from forming some test cases**. This one is sort of related to one of my earlier points as it helps me seeing my destination before starting. It obviously makes the end result safer but as a bonus it often helps designing and documenting the code. I am actually surprised so few developers can appreciate this point of view.
- Another fresh one for me which is to **avoid "future programming"**. Start small and grow later. In my earlier days while coding anything I wanted to make it perfect from the very first day, cover all the possible edge cases and almost prepare it for my descendants to use. With time I realised it often leads to overcomplicated codebase, high time consumption and in most cases my program is doing everything and nothing at the same time!
	
So here, take any of these for yourself if you feel like it. These I have found insanely useful on my programming path but forming them is not an overnight change. The best and only way to implement a habit in your life is to just start using it and it will settle down before you realise.
Ah, do not forget to let me know about other habits that worked for you!
