---
Title: We went to NorDevCon...
Description: Some of my notes from the development themed conference in Norwich.
Author: Bartlomiej Karalus
Date: 01/03/2017
Position: 9994
---

I was lucky to go to NorDevCon with Codeweavers this year. Make sure to check out the full relation on our 
<a href="https://codeweavers.net/developer-blog/a-weavers-experience-of-nordevcon" title="Codeweavers Developer Blog - NorDevCon">company blog</a>.

<div class="image-placeholder" data-src="%base_url%/assets/blog/nordevcon2017.png">
    <img src="%base_url%/assets/thumbs/blog/nordevcon2017.png" class="mini" />
</div>