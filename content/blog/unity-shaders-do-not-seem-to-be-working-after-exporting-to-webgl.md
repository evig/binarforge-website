---
Title: Unity Engine Shaders do not seem to be working after exporting to WebGL
Description: I could not see my amazing shaders after exporting the project.
Author: Bartlomiej Karalus
Date: 15/03/2016
Position: 9997
---

The other day, my ultimate shader was polished and ready, working like a dream in editor preview mode and then… webgl version did not pick it up. It does not work and the thrill is gone.

After fairly short investigation, these two steps appear to help. First of all, in the Edit → Project Settings → Graphics there is a list of shaders „always included”. We need to include all our shaders in this list:

<div class="image-placeholder" data-src="%base_url%/assets/shadery_webgl_1.png">
    <img src="%base_url%/assets/_shadery_webgl_1.jpg" class="mini" />
</div>

(there are some default one already)

I was pretty sure it will fix the issue but no... still no magic on the screen.
I went to the "Player Settings" this time, and in the HTML5 settings I checked the „Preload Shaders” option.

<div class="image-placeholder" data-src="%base_url%/assets/shadery_webgl_2.png">
    <img src="%base_url%/assets/_shadery_webgl_2.jpg" class="mini" />
</div>

As for me, it solved the problem without getting much into the details of why it required those two particular steps.

P.S. The shader itself was not really that exciting in the end... simple screen colour manipulation.