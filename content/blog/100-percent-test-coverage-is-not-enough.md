---
Title: Never enough of testing...
Description: You shall never be fully covered with tests...
Author: Bartlomiej Karalus
Date: 07/02/2017
Position: 9995
---

The place I am currently working at, as any other decent company, is really focused on the quality of its products.
One of the company virtues is to make sure the software being created is reliable. Full test coverage is one of the ways to assure everything is working as it was designed to.

But... one day while taking a break in our kitchen, I came to realize that this world is not as perfect as one may think of it and even the 100% test coverage does not make the company flawless.
How ironic it is to see those basic things not working correctly... easily to be avoided by - YES - some sort of testing. 

<div class="image-placeholder" data-src="%base_url%/assets/blog/100-percent-test-coverage-is-not-enough.png">
    <img src="%base_url%/assets/thumbs/blog/100-percent-test-coverage-is-not-enough.png" class="mini" />
</div>

We obviously did not create those microwaves but maybe that is the lesson! Otherwise how can we dare to say that everyting in our company is fully tested?
I am not sure whether unit tests actually exist in microelectronics manufacturers minds. Though the next time I will need a microwave I would be tempted to reprogram my own. Just to make sure.

<pre><code class="javascript">module.exports = {
    arrayNumberSwap: function(arr, i, j){
        if(i === j)
            return;
            
        arr[i] = (arr[j] += arr[i]) - arr[i] + (arr[j] = arr[i]) - arr[i];
    }
}</code></pre>