---
Title: Is being a game programmer undeniably perfect, dream job?
Description: Is it really a job of gods?
Author: Bartlomiej Karalus
Date: 02/03/2016
Position: 9998
---

<div class="image-placeholder" data-src="%base_url%/assets/blog/is-being-a-game-programmer-a-dream-job.png">
    <img src="%base_url%/assets/thumbs/blog/is-being-a-game-programmer-a-dream-job.png" class="mini" />
</div>

I always wanted to make video games. I still do and the fact of not being significantly successful in this field does not really matter to me. It is a real hobby whether or not it is capable of becoming a viable source of income.

It helped me while making harsh decision about sort of deserting the gamedev industry after not really long period of two years. Some would say that I did not even taste it properly and sadly that is true.

In my personal feeling, over the last decade or two getting into gamedev got hyped up to a godmode  status. All those young not-yet-developers are talking about joining big game companies or bringing out the new hot titles and becoming real superstars. Note that I am not criticizing this phenomenon here. Every dream is absolutely worth of chasing and that is just a quick retro why it did not work for me in particular. But cutting it short, I have joined a company specialising in web apps and this actually turned out to be pretty good life decision.

Thanks to my second hobby, which always was web programming, I found my new  job quite easy, while still full of learning space and challenges. Alright, these might not include optimizing the geometry for mobile rendering, obviously it is more like dealing with large businesses and customers.

Someone might say that leaving gamedev job and going to a software house is like degrading. I do not think that way anymore. Working for a game company at the end of the day was all about creating someone else’s vision. It will always be that way, unless you are running your own moreless independent studio, which scenario would probably be even more difficult to keep up with.

As a contrast, I have joined this well organized, web app oriented company, organized within a little bit more predictable industry. It quickly allowed me to regain sort of balance in my life and created significant space for independent creativity.

Yes, when I was actively working as a game programmer I could not create anything outside of my working hours, my brain was simply not capable of producing anything else. Not even mentioning here overtimes or pre-realase stress, but I am not trying to say that it is a bad type of job. Canvassing might be a bad type of job. Game programming is not! I absolutely loved the time being there and still missing these game-oriented minds I was working with!

What makes it a „pretty good life decision” then? I would say having a stable, challenging and developing job with cool environment together with slack creativity and fresh mind at the end of the day, allows me to chase my initial hobby and take a peaceful dive into making games anytime I need. Let it be and see what happens!