---
Title: Invisible changes for the better
Description: To change, without changing? What...
Author: Bartlomiej Karalus
Date: 05/11/2017
Position: 9991
---

During a recent online course we run, we started talking about code refactors. It had some questions spawned and I realised what is obvious to me, might still be a mystery to someone else. Especially that the course was targeted at beginners learning Java Script. So here is what I think refactoring is, based on some of my experience so far. 

The main goal of code refactoring is to change, but not to change. Now, I am aware this sounds like nonsense so to stress it, we need to give it some context. We want to change the code of our program without changing its end result. Simply said, simply done? Not exactly. But the most important question is Why. Sadly, this questions hardly ever meets an answer good enough to make any programming team start using this technique. Personally, I stumbled upon this term as far as in my...fourth job, if I recall correctly. 

<div class="image-placeholder" data-src="%base_url%/assets/blog/invisible-changes-for-the-better.png" style="width: 50%">
    <img src="%base_url%/assets/thumbs/blog/invisible-changes-for-the-better.png" class="mini" />
</div>

As many things, it comes with a little bit of experience by which one can afford slightly different perspective. Say you have just started your programming <s>adventure</s> career. You might find yourself thinking, why in the world would I change something that already works fine? 
Yes you have spent a significant amount of time putting all these codes together, making them working, perhaps even demonstrated the results to some stakeholders. So from now on, you could only potentially break things, right? 

###That is where the perspective comes in.###

A piece of code which happens to work today, might not work tomorrow or could become a pain in the ass to modify in the future. Jim Benson once said that software is being Done like lawn is being Mowed. Following this grass metaphor, one can say that if you sow the seed unevenly, it shall grow unevenly. Therefore, I believe spending this little extra effort to keep your code nice and clean is one of the best future investments in your codebase. That is the point of refactoring as a periodical process, to keep your code clean, maintainable and most importantly, understable to anyone now and at any point in time.

###But what sort of changes?###

When it finally comes to it, what do we change, how do we make the change? Here are some popular motivation points to start refactoring an existing code:
- Simple variable or methods renaming to improve overall readability
- Grouping functionality into logical classes or modules (files)
- Simplifying dependencies between objects in your existing architecture
- Making code test-friendly or testable, primarily
- Enhancing performance by optimising algorithms and/or library swaps, upgrades
- Preparing your codebase for using new upcoming language improvements (e.g. php 5.6 → 7)

There would be more examples like these, but my point is that none of them should result in any visible change of the program’s behaviour. If there are such changes, something on the way has gone wrong and you should probably take a deeper look into it.

###Good changes only###

So in general, we are talking here about changing for the better. It might happen that some changes might be unnecessary or redundant, showing no results at all. We should avoid future programming,  but at the same time we should think about how the code we are working on today could be useful and readable in the future. Most of the times, refactoring seems like a natural step after something known as „the proof of concept”, which usually is a pile of quickly-made and sticked together codes, of which main goal was to prove an idea or demonstrate an ability to solve particular problem. But obviously, if there is a plan on continuing working on it there is a need to normalise it and make it „the right way”. In reality, though, many dev teams happily skip this phase condemning anyone in the future to work on this little nightmare that once has been created. 

###Unit testing is your ally###

As a closing note, I would like to give special mention to unit tests. They already proved to be useful here and there, but they make perfect sense in the context of code refactoring. If your code is properly wrapped with some test suites, you can make any amount of changes to your code and quickly catch the behaviour change. How safe is that approach? I say, safer than anything. 

Hopefully these notes will serve as a reminder of how important code refactor might be for any ongoing software project.