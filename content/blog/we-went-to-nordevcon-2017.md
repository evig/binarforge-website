---
Title: We went to NorDevCon...
Description: Some of my notes from the development themed conference in Norwich.
Author: Bartlomiej Karalus
Date: 01/03/2017
Position: 9994
---

<div class="image-placeholder" data-src="%base_url%/assets/blog/we-went-to-nordevcon-2017.png">
    <img src="%base_url%/assets/thumbs/blog/we-went-to-nordevcon-2017.png" class="mini" />
</div>

I was lucky to go to NorDevCon with Codeweavers this year and I was pretty impressed. It was definitely the time well spent and I am already looking forward to getting there next year.
But first there are is still few more interesting ones to visit this year.

As of now, please make sure to check out the full relation from this one on our 
<a href="https://codeweavers.net/developer-blog/a-weavers-experience-of-nordevcon" title="Codeweavers Developer Blog - NorDevCon">company blog</a>.