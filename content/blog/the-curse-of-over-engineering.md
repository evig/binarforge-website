---
Title: The curse of over engineering
Description: We all do like to complicate things, do not we?
Author: Bartlomiej Karalus
Date: 04/06/2017
Position: 9993
---
 
<div class="image-placeholder" data-src="%base_url%/assets/blog/the-curse-of-over-engineering.png">
    <img src="%base_url%/assets/thumbs/blog/the-curse-of-over-engineering.png" class="mini" />
</div>

One day, a man named Jim wanted to make sandwich. He has bought all the ingredients and started creating a *prototype*. 
After few initial bites he realised he does not really like the taste of the lettuce he just brought home. The decision has been made, that growing his own lettuce in the garden is what man really needs. The next few weeks have been spent preparing the garden, clearing the soil and digging it all over. Jim was tired but satisfied and excited about the idea of growing vegetables on his own. At this point he realised he has never done it before. Even though growing vegetables seems relatively easy, Jim decided to see his friend, who happened to be an experienced home grower and asked him for some hints. Jim's friend was really enthusiastic about his new hobby, though he could not help the fact that it was little too late in the year to start growing anything. 

"It is all right Jim. You put amount of great work in your garden and it will be ready to start as soon as the winter is gone." 
  
Jim felt slightly disheartened but waited patiently next few months until springtime. Then the summertime came and when even more minor problems were tackled on the way, the fresh, green lettuce was grown, collected and ready. Jim invitied few more friends and full of pride, provided everyone with fresh, home made sandwiches. During the consumption a converstation took place and someone completely unaware said:

"The sandwiches are great Jim, but the vegetables sold in the shops are nothing good nowadays..."

If Jim have ever existed, it would be the point at which he learns a valuable lesson. It took me quite few projects and no lettuce ever grown to see it for myself. But are not we all developers guilty of being Jim from time to time? 

Remember this project that you wanted to be special? No ready made solutions, no heavy third-party libraries, all code in-house?   Yes, that is the one. How many of them were lucky enough to become this "special sandwich"? Handshake from me, if you actually did manage to do so. 

Even so, has anyone ever noticed the amount of work that you put in? You are one lucky person to experience this unique level of accomplishment. I am guilty of spending significant amount of time trying to, let's face it, reinvent the wheel. 
 
<div class="image-placeholder no-stretch" data-src="%base_url%/assets/over-engineering.jpg">
    <img src="%base_url%/assets/_over-engineering.jpg" class="mini" />
</div>  

This is great for learning though, to not demonize this phenomenon completely. But it might be dangerous for real life softawre development companies in particular. Sometimes people start to think that no tool in the world suits their needs and happily start developing their own version of the tool currently required. This can work if used wisely and I happened to work in a company which was quite successful at building most of their tools themselves. 

On the other hand, when things are getting out of control and you potentially start to reprogram everything, including the company's coffee machine, it can only lead to enormous technical debt, most likely impossible to ever get out of. Tools is only an example but surely there are some real life examples of companies focused too much on some unnecessary details of the product, quickly resulting in going off the rails.

So, pride put aside and prototype fast. As the time passes, there is more and more great code, modules, packages available. Make use of them, appreciate someone's else hard work and achieve your goals quicker is my motto for today.