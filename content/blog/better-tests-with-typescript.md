---
Title: Better tests with TypeScript
Description: It does make sense to use the whole potential of TypeScript, does not it?
Author: Bartlomiej Karalus
Date: 31/08/2017
Position: 9993
---

When I used to work on a TypeScript project at some point in the past, I have observed some unused potential there. Usually TypeScript gets compiled (or shall I say transpiled) into plain JavaScript, so the unit tests we have been writing were all created in JavaScript respectively. It makes sense to keep your tests in the same language as the source code. But I figured it would be so much better to have unit tests in the initial technology which was TypeScript as I mentioned before.

This technology was created to take JavaScipt to its limits and beyond. But why skipping these possibilites in unit tests? I can see no good reasons to for that that so I started digging. The answer came back quickly as someone has already [https://github.com/TypeStrong/ts-node done the job]. After quick research I have found this nice npm module called ts-node. 

This particular module was made to allow NodeJS executing TypeScript files which sounds exactly like something I could use. But it is not all, a proper testing framework to support this trick will be necessary too.

Let me form some assumptions first. To picture the idea I am going to create a class 
simulating a very simple Shop. It should allow us to initialise a list of products (name, price) and expose some interface to add items to the basket and check the total. Such a simple setup allows embedding some clear and easily testable logic within the Shop class.

Start by creating a folder for new project and using module manager of choice (npm, yarn, ...) install following modules:

npm init –force
npm i -D typescript mocha chai ts-node
npm i -D @types/chai @types/mocha

*init* command with forcing flag would create a default package.json file and then simply fetch TypeScript, Mocha, Chai, and Ts-Node mentioned at the beginning. For my comfort I also grab two @types modules which help my editor with annotations and hints in TypeScript projects. These type definitions modules are not required for this experiment.

So what happened here:
- TypeScript - I want write my source code and tests in this technology 
- Mocha & Chai - My favourite testing frameworks which allow writing more verbose and descriptive test suites. Chai in particular is an assertion library and Mocha's configuration flexibility is great for usage with ts-node.
- Ts-Node is a modul allowing NodeJS understanding and executing TypeScript files
- @types/chai and @types/mocha are type defnition files to provide code hints and annotations to the libraries being used in my project.

As a loyal TDD believer I will start my coding from writing the actual tests:

[a lot of code]

This is just one example test which makes sure that Shop class supports "3 for 2" discounts which in this case is the core logic of my dummy class. You can find the full suite on my github.

What is here to be noticed so far?
- Mocha gives you this nice way to fully describe all your scenarios
- Chai allows you to come up with verbose assertions on the results
- Some would say there is not much space for all the cool things from TypeScript but this is just a demonstration. I am sure there will be way more benefits in a real codebase.
- Note how this test suite automatically becomes the documentation for your class. Its easy to see all the usages and scenarios of the code. And thats for free!

The next step would be to figure out how to connect Mocha and Ts-Node to run the tests in TypeScript flavour. Quick look into the docs brings a relief as the whole things is extremely easy. I modified the default test script in package.json:
 
(...)
"scripts": {
   "test": "mocha -r ts-node/register src/**/*.test.ts || exit 0"
},
(...)

Mocha framework pointed to the Node's TypeScript interpreter allows me to smoothly run TypeScript tests as Mocha runs on NodeJS as well, right? My addition here is to hide all the potential and unnecessary error output. I obviously want to run files with test.ts extension only. Now all I need to do is to run npm script:

npm test

and observe the results. The test run will obviously fail as I have not even started writing the actual code yet. Which brings me to this final step.
Below is my solution to this "problem" but I strongly encourage you to try you own!

[kodu]

The whole source code can be found on my github here. 

So here is this very simple way to make your unit tests better and make your technologies match in your web app projects. Low cost of implementation and quite few nice rewards visible straightaway make it really worth of trying and doing so I strongly recommend. 
Let me know if it did or did not work for you and share your own conclusions in the comments below.

As a final note I would like to give an example or two on how I make use of the @types modules. In my case, VS Code picks it up automatically and provides you with helpful code hints!

<div class="image-placeholder" data-src="%base_url%/assets/blog/nordevcon2017.png">
    <img src="%base_url%/assets/thumbs/blog/nordevcon2017.png" class="mini" />
</div>