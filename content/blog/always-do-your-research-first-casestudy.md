---
Title: Always do your research!
Description: Perfect title might need some thoughtful moments first.
Author: Bartlomiej Karalus
Date: 03/10/2015
Position: 9999
---



Sometimes we learn the hard way. As some might know, I once have been involved in creating an arcade/adventure mobile game. During design process we came up with a nice and short title which sound was fitting perfectly into theme. Yeah, cute character’s name fulfills its cute graphics and so on.

So we have decided quickly, and happily gave up on any further research.

<strong>WRONG.</strong>

Always do your research, that was to be the conclusion. Just to remind, the game’s title was <em>Willy</em>. Funny thing is that I did not realized what that can actually mean through the whole development time.

The actual revelation took place during some later conversation about the game with english speaking colleague, who asked me: “Err, did you google that before?”.
<div class="image-placeholder" data-src="%base_url%/assets/blog/always-do-your-research-first-casestudy.png">
    <img src="%base_url%/assets/thumbs/blog/always-do-your-research-first-casestudy.png" class="mini" />
</div>
Yes I did but maybe something like 14 months late. It is not actually a bad or insulting name, but still I reckon that a game like that should have some safer name. Imagine you pick a title that sounds awesome, fits the theme in an unbelievable way and then it turns out, that in one of the languages on the other side of the world it means e.g. “Stinky piece of boredom” or something like “The goat is a bad touch”.

I hope I can avoid more situations like this in future, thanks to that one.