---
Title: Work with me
Position: 4
---

<div class="image-placeholder" data-src="%base_url%/assets/work-with-me.png">
    <img class="mini" src="%base_url%/assets/_work-with-me.png" alt="Work with BinarForge" />
</div>

Apart from my job I am also providing some occasional freelance services.

I am interested in many areas of software development but following are those I have got professional experience with:
- Web & Mobile Development (HTML, CSS, JavaScript, Angular, NodeJS, Gulp, ReactNative, Unit Testing, PHP)
- Game Development (C#, Unity3D, Phaser.js, MonoGame)

Make sure to checkout my <a href="%base_url%/pages/timeline"><em>Timeline</em></a> to check what I have been dealing with in last years.
Feel free to contact me anytime, I am always happy to work, help or explain.

<a href="mail:contact@binarforge.com">contact@binarforge.com</a>