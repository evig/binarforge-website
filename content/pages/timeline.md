---
Title: Timeline
Position: 1
---

<link rel="stylesheet" href="%base_url%/assets/css/style.css"> 
<script src="%base_url%/assets/js/modernizr.js"></script>

<p>My work background is really a magical mix of <strong>web development</strong> and <strong>games programming</strong>. The reason for that is simple as I simply could not decide which hobby I love more!</p>

<section id="cd-timeline" class="cd-container">
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/exclamation-alert-sign-on-reminder-daily-calendar-page.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>Senior Web Developer</h2>
            <h3>Zonal Retail Data Systems, Staffordshire, UK</h3>
            <p>Getting involved in development of iOrder platform providing web and mobile ordering for chain restaurants across the UK.</p>
            <span class="cd-date">Jun 2017 - Now<</span>
        </div>
    </div>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/calendar-tool-for-time-organization.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>Web Developer</h2>
            <h3>Codeweavers Ltd, Staffordshire, UK</h3>
            <p>Moved to the UK and joined this well kept software house. Spent some time developing both backend and frontend though by the end of this adventure I was deliberately more focused on frontend technologies.</p>
            <span class="cd-date">Nov 2015 - May 2017<</span>
        </div>
    </div>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/calendar-tool-for-time-organization.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>Unity Developer</h2>
            <h3>Dardanelle Studio, Cracow, POLAND</h3>
            <p>Joined this local studio for a short period of time but my plans have changed rapidly. During my time I have created a casual mobile game working closely with a team of graphic designers.</p>
            <span class="cd-date">August 2015 - Oct 2015</span>
        </div>
    </div>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/calendar-tool-for-time-organization.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>Unity Developer</h2>
            <h3>EveryDayIPlay, Cracow, POLAND</h3>
            <p>Thanks to my general passion for games and some initial experience I have had the honour of joining those guys to work on a game titled „Heroes of Paragon”. Certainly a great experience and priceless gamedev world lesson.</p>
            <span class="cd-date">Aug 2014 – Jun 2015</span>
        </div>
    </div>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/exclamation-alert-sign-on-reminder-daily-calendar-page.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>Game Developer</h2>
            <h3>TreStory, Lodz, POLAND</h3>
            <p>Independent development and release of Android mobile game titled „Willy”. Dreams of games programming kicked in right after my graduation. Most people were simply grabbing their first jobs whilst I have spent few months trying to create my own job.</p>
            <span class="cd-date">Apr 2014 - Jul 2014</span>
        </div>
    </div>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/trophy-on-daily-calendar-page-interface-symbol-of-the-contest-day.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>Graduation</h2>
            <h3>Lodz University of Technology, Lodz, POLAND</h3>
            <p>Engineering Thesis titled <em>„Calculations on physical algorithms with GPU support”.</p>
            <span class="cd-date">Mar 2014</span>
        </div>
    </div>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/calendar-tool-for-time-organization.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>PHP Developer</h2>
            <h3>Pakulski.net company, Lodz, Poland</h3>
            <p>Working with popular eCommerce scripts, WordPress and Symfony 2 based client websites. Full time when possible but dreams of graduation were still in the bigger picture.</p>
            <span class="cd-date">Mar 2013 – Sep 2013</span>
        </div>
    </div>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/calendar-tool-for-time-organization.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>Web Developer</h2>
            <h3>PPHU PINEA, Lodz, Poland</h3>
            <p>Decided to turn my new hobby into an actual job while still studying. Used my semester break to find this job in a local furniture company during my summer holiday and was able to maintain and develop a Symfony based e-commerce script and a bunch of company's microsites. This allowed me to get my first modest experience in software development outside my bedroom.</p>
            <span class="cd-date">Jul 2012 – Sep 2012</span>
        </div>
    </div>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/exclamation-alert-sign-on-reminder-daily-calendar-page.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>Fresh, optimistic Student</h2>
            <h3>Lodz University of Technology, Lodz, Poland</h3>
            <p>My new subject of interest brought me right into the Uni. I was happy to have one of the best polish technical schools in my home city.
            At this point of time I was pretty sure I alredy know everything about "coding". Except I did not have a clue.</p>
            <span class="cd-date">Oct 2010</span>
        </div>
    </div>
    </div>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-icon">
            <img src="%base_url%/assets/svg/exclamation-alert-sign-on-reminder-daily-calendar-page.svg">
        </div>
        <div class="cd-timeline-content">
            <h2>Curious Person</h2>
            <h3>Home, Lodz, Poland</h3>
            <p>It happened. Came across the "programming" term while playing video games instead of doing my homework.<br/> I had to know how it works and how software is being created. Will I ever regret?</p>
            <span class="cd-date">2008, ~High School</span>
        </div>
    </div>
</section>

<small><em>Timeline icons designed by Freepik from Flaticon</em></small>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="%base_url%/assets/js/main.js"></script>
