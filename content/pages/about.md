---
Title: About
Position: 5
---

Welcome stranger!

The name of this website might sound a little bit like overkill, as I am going to be honest with you, it is not really a forge. I am yet another developer and I do believe that software crafting is quite similar to, err, crafting the swords. At the end of the day you produce some mighty, shiny weapon that can conquer the world, you know…

Anyway, this is my place to share my occassional thoughts from the world of software development. I hope you will enjoy some of the readings here.

p.s. If you are crazy enough to be fluent in polish language make sure you check out my native version of the <a href="http://kuzniabinarna.pl" title="polish version of the blog">blog</a>.

